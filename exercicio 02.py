# 1) Escreva um script em Python que substitua o caractere “x” por espaço considerando a
# seguinte frase:
# “Umxpratoxdextrigoxparaxtrêsxtigresxtristes”

# RESPOSTA:

# frase = "Umxpratoxdextrigoxparaxtrêsxtigresxtristes"
# frase = frase.replace("x", " ")

# print(frase)

# ======================================================================================================

# 2) Escreva um programa que receba o ano de nascimento, e que ele retorne à geração
# a qual a pessoa pertence. Para definir a qual geração uma pessoa pertence temos a
# seguinte tabela:

# Geração        Intervalo

# Baby Boomer -> Até 1964
# Geração X   -> 1965 - 1979
# Geração Y   -> 1980 - 1994
# Geração Z   -> 1995 - Atual

# Para testar se seu script está funcionando, considere os seguintes resultados esperados:

# • ano nascimento = 1988: Geração: Y
# • ano nascimento = 1958: Geração: Baby Boomer
# • ano nascimento = 1996: Geração: Z

# RESPOSTA

# var_nasc = int(input("Qual ano vc nasceu? "))

# if var_nasc >= 1995:
#     print(f"""
#     Ano nascimento = {var_nasc}: Geração: Z
#     """)
# elif var_nasc >=1980:
#     print(f""""
#     Ano nascimento = {var_nasc}: Geração: Y
#     """)
# elif var_nasc >=1965:
#     print(f""""
#     Ano nascimento = {var_nasc}: Geração: X
#     """)
# else:
#     print(f""""
#     Ano nascimento = {var_nasc}: Geração: Baby Boomer
#     """)

# ======================================================================================================

# 3) Escreva um script em python que represente uma quitanda. O seu programa deverá
# apresentar as opções de frutas, e a cada vez que você escolher a fruta desejada, a fruta
# deverá ser adicionada a uma cesta de compras.

# Menu principal:

# Quitanda:
# 1: Ver cesta
# 2: Adicionar frutas
# 3: Sair

# Menu de frutas:
# Digite a opção desejada:
# Escolha fruta desejada:
# 1 - Banana
# 2 - Melancia
# 3 - Morango

# Digite à opção desejada: 2
# Melancia adicionada com sucesso!

# Os menus 1 e 2 deverão retornar ao menu principal após executar as suas tarefas.
# Você deverá validar as opções digitadas pelo usuário (caso ele digitar algo errado, a mensagem:
# Digitado uma opção inválida

# O programa deverá ser encerrado apenas se o usuário digitar a opção 3.

cesta = []

while True:  
    print("""
    Quitanda:
    1: Ver certa
    2: Adicionar frutas
    3: Sair
    
    """)

    var_opt = input("Escolha uma opcao: ")

    if var_opt == "1":
        if len(cesta) < 1:
            print("A cesta esta vazia")
        else:
            print(cesta)

    elif var_opt == "2":
        while True:
            print("""
            # Menu de frutas:
            1 - Banana
            2 - Melancia
            3 - Morango
            x - Voltar
            
            """)
            var_fruta = input("Digite a opção da fruta desejada:")

            if var_fruta == "1":
                cesta.append("Banana")
                break
            elif var_fruta == "2":
                cesta.append("Melancia")
                break
            elif var_fruta == "3":
                cesta.append("Morango")
                break
            elif var_fruta == "x":
                break
            else:
                print("Opcao invalida")
    elif var_opt == "3":
        print("Volte sempre")
        break
    
    else:
        print("Opcao invalida")

# ======================================================================================================

# 4) Altere o exercicio numero 3 para adicionar o preço dos itens comprados, mantendo uma conta do valor
# total gasto nas compras, e no fim, imprima o valor total e os itens na cesta de compras.

