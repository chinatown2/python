# 1) Em muitos programas, nos é solicitado o preenchimento de algumas informações como
# nome, CPF, idade e unidade federativa. Escreva um script em Python que solicite as
# informações cadastrais mencionadas e que em seguida as apresente da seguinte forma:

# -----------------------------
# Confirmação de cadastro:
# Nome: Guido Van Rossum
# CPF: 999.888.777/66
# Idade: 65
# -----------------------------

# RESPOSTA 

var_nome = input("Nome: ")
var_cpf = input("CPF: ")
var_idade = input("Idade: ")

# Print 01
# print("-----------------------------")
# print("Confirmação de cadastro:")
# print("Nome:", var_nome)
# print("CPF:", var_cpf)
# print("Idade:", var_idade)
# print("-----------------------------")

# Print 02
# print("-----------------------------")
# print("Confirmação de cadastro:")
# print(f"Nome: {var_nome}")
# print(f"CPF: {var_cpf}")
# print(f"Idade: {var_idade}")
# print("-----------------------------")

# Print 03
print(f"""
-----------------------------
Confirmação de cadastro:
Nome: {var_nome}
CPF: {var_cpf}
Idade: {var_idade}
-----------------------------
""")


# 2) Escreva um script em Python que receba dois números e que seja realizado as seguintes
# operações:
# • soma dos dois números
# • diferença dos dois números
# O resultado deverá ser apresentado conforme a seguir - no exemplo foram digitados os números
# 4 e 2:

# ------------------------------
# Soma: 4 + 2 = 6
# Diferença: 4 - 2 = 2
# ------------------------------

# RESPOSTA

# Variaveis 01
# var_num1 = input("Insira o primeiro numero: ")
# var_num2 = input("Insira o segundo numero: ")

# var_soma = int(var_num1) + int(var_num2)
# var_dif = int(var_num1) - int(var_num2)

# Variaveis 02
var_num1 = int(input("Insira o primeiro numero: "))
var_num2 = int(input("Insira o segundo numero: "))

var_soma = (var_num1) + (var_num2)
var_dif = (var_num1) - (var_num2)

# Print 01
# print("------------------------------")
# print("Soma:", var_num1, "+", var_num2, "=", var_soma)
# print("Diferenca:",var_num1, "-", var_num2, "=", var_dif)
# print("------------------------------")

# Print 02
# print(f"""
# ------------------------------
# Soma: {var_soma}
# Diferença: {var_dif}
# ------------------------------
# """")

# Print 03
print(f"""
------------------------------
Soma: {var_num1} + {var_num2} = {var_soma}
Diferença: {var_num1} - {var_num2} = {var_dif}
------------------------------
""")